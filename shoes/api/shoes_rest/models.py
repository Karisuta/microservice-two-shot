from django.db import models
from django.urls import reverse

# Create your models here.
class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, null=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=40, null=True)
    model_name = models.CharField(max_length=40, null=True)
    color = models.CharField(max_length=40, null=True)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        null=True,
    )
