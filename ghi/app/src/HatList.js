import { Link } from 'react-router-dom'
import React from 'react'


function HatDetail(props) {
    console.log("HatDetail");
    console.log(props);
    return (
        <div className='col'>
            {props.list.map(hat => {
                return (
                  <div key={hat.href} className="card mb-3 shadow">
                    <img src={hat.picture_url} className="card-img-top img-fluid" width="100" height="100" />
                    <div className="card-body">
                      <h5 className="card-title">{hat.style_name}</h5>
                      <h6 className="card-subtitle mb-2 text-muted">
                        {hat.fabric}
                      </h6>
                      <h6 className="card-subtitle mb-2 text-muted">
                        {hat.color}
                      </h6>
                      <button onClick={() => deleteHat(hat.href)} type="button" className="btn btn-outline-danger">Delete Hat</button>
                    </div>
                  </div>
                );
            })}
        </div>
    );
}

const deleteHat = async (href) => {
    fetch(`http://localhost:8090${href}`, {
        method: 'delete',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(() => {
        window.location.reload()
    })
}

class HatList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hatDetails: [[],[],[]],
        };
    }


    async componentDidMount() {
        // url for the hats api
        const url = "http://localhost:8090/api/hats/";

        try {
            // this gets the data from the hats api
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                // creates a list for all the resquests
                // and adds all the requests to it
                const requests = [];
                for (let hat of data.hats) {
                    const detailUrl = `http://localhost:8090${hat.href}`;
                    requests.push(fetch(detailUrl));
                }

                // Wait for all of the requests to finish simultaneously
                const responses = await Promise.all(requests);

                const hatDetails = [[], [], []];
                let i = 0;
                for (const hatResponse of responses) {
                    if (hatResponse.ok) {
                        const details = await hatResponse.json();
                        console.log(details)
                        hatDetails[i].push(details);
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(hatResponse);
                    }
                }
                // set the state
                this.setState({hatDetails: hatDetails})
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <div className="container">
                <h2>Hats!</h2>
                <a href={"/hats/new"} className="btn btn-primary btn-lg mb-3 active" role="button" aria-pressed="true">Create Hat</a>
                <div className='row'>
                    {this.state.hatDetails.map((hatLists, index) => {
                        return (
                            <HatDetail key={index} list={hatLists} />
                        );
                    })}
                </div>
            </div>
        );
    }
}

export default HatList;
