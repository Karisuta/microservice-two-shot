import React, { useState, useEffect } from 'react';


export default function ShoeList() {
    const [shoes, setShoes] = useState([]);

    async function LoadShoes() {
    const response = await fetch("http://localhost:8080/api/shoes/");
    if (response.ok) {
    const data = await response.json();
    setShoes(data.shoes);
    }
    }
useEffect(() => {
    LoadShoes()
})

const deleteShoe = async (id) => {
  fetch(`http://localhost:8080/api/shoes/${id}`, {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(() => {
    window.location.reload()
  })
}


return (
    <>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Manufacturer</th>
          <th>Model Name</th>
          <th>Color</th>
          <th>Photo</th>
        </tr>
      </thead>
      <tbody>
        {shoes.map((shoes) => {
          return (
            <tr key={shoes.id}>
              <td>{ shoes.manufacturer }</td>
              <td>{ shoes.model_name }</td>
              <td>{ shoes.color }</td>
              <td className="w-25">
                <img
                  className="img-fluid img-thumbnail"
                  src={shoes.picture_url}
                  width="100"
                  height="100"
                />
            <td><button onClick={() => deleteShoe(shoes.id)} type="button" className="btn btn-outline-danger">Delete Shoe</button></td>
            </td>
            </tr>
          );
        })}
      </tbody>
    </table>
    <a href={'/shoes/new'} className="btn btn-primary btn-lg active" role="button" aria-pressed="true">Create Shoe</a>
    </>
  );
}
