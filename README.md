# Wardrobify

Team:

* Person 1 - Melody Oberg: Shoes Microservice
* Person 2 - Jesse Preble: Hats Microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

I created a 'Shoe' model with a foreign key that correlates to the 'Bin' model in the 'Wardrobe' database. I implemented a function in my 'poller.py' that periodically checks the 'Wardrobe' database for newly created bins.
With this functionality, I can assign a bin to a shoe object in my application.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

The model outlines a hat object and a corresponding locationVO. Because a hat is dependent on there being locations, first a poller is created that polls the wardrobe for the locations that have been created. Then, it takes that data and creates a locationVO object which can then be used to assign a location to a hat when a hat is created or updated.
